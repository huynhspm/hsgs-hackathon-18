"use strict";
Object.defineProperty(exports, "__esModule", { value: true });

function Shift(a, n) {
  for (let i = 0; i < n; ++i) a.unshift(a.pop());
  return a;
}

function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    const x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
  return a;
}

const Maidel = {
  default(props = { size: 4, rate: 2 }) {
    const size = props.size * 2;
    const rate = props.rate;
    let t_odd = [];
    let t_eve = [];
    for (let i = 0; i <= size; ++i) i % 2 === 1 ? t_odd.push(i) : t_eve.push(i);
    t_odd = shuffle(t_odd);
    t_eve = shuffle(t_eve);

    let fir = [];
    for (let i = 0; i < t_odd.length; ++i) {
      fir.push(t_odd[i]);
      fir.push(t_eve[i]);
    }

    let odd = [];
    let eve = [];
    for (let i = 0; i < size; ++i) {
      const a = Array.from(fir);
      if (i % 2 === 0) odd.push(a);
      else eve.push(a);
      fir = Shift(fir, 1);
    }
    odd = shuffle(odd);
    eve = shuffle(eve);

    let board = [];
    for (let i = 0; i < odd.length; ++i) {
      board.push(odd[i]);
      if (eve[i] !== undefined) board.push(eve[i]);
    }

    // console.table(board);

    for (let i = 0; i < size; ++i) {
      for (let j = 0; j < size; ++j) {
        const print = Math.floor(Math.random() * rate);
        board[i][j] = -board[i][j];
        if (print !== 0) board[i][j] = null;
      }
    }
    // console.table(board);
    return { board };
  },
  actions: {
    async Place(state, { x, y, val }) {
      let board = state.board;
      const size = board.length;
      if (val === null) return { board };
      if (val < 0 || val > size) throw new Error("invalid");
      board[x][y] = val;
      return { board };
    },
    async Reset(state) {
      let board = state.board;
      const size = board.length;
      for (let i = 0; i < size; ++i)
        for (let j = 0; j < size; ++j)
          if (board[i][j] > 0 && board[i][j] <= size) board[i][j] = null;
      return { board };
    }
  },
  isValid(state) {
    const piles = state.board;
    if (!(piles instanceof Array)) return false;
    for (const pile of piles) if (!(pile instanceof Array)) return false;
    return true;
  },
  isEnding(state) {
    const board = state.board;
    const size = board.length;
    for (let i = 0; i < size; ++i) {
      let mark = Array(size + 1).fill(false);
      for (let j = 0; j < size; ++j) {
        if (!Number.isInteger(board[i][j])) return null;
        let k = Math.abs(board[i][j]);
        if (mark[k]) return null;
        else mark[k] = true;
      }
      for (let k = 1; k <= size; ++k) if (!mark[k]) return null;
    }
    for (let j = 0; j < size; ++j) {
      let mark = Array(size).fill(false);
      for (let i = 0; i < size; ++i) {
        if (!Number.isInteger(board[i][j])) return null;
        let k = Math.abs(board[i][j]);
        if (mark[k]) return null;
        else mark[k] = true;
      }
      for (let k = 1; k <= size; ++k) if (!mark[k]) return null;
    }

    for (let i = 0; i < size; ++i) {
      for (let j = 1; j < size; ++j) {
        if ((board[i][j] + board[i][j - 1]) % 2 === 0) return null;
      }
    }

    for (let j = 0; j < size; ++j) {
      for (let i = 1; i < size; ++i) {
        if ((board[i][j] + board[i - 1][j]) % 2 === 0) return null;
      }
    }

    return "won";
  }
};

exports.default = Maidel;
