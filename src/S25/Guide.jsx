import React from "react";
class Guide extends React.Component {
  render() {
    return (
      <div className="s25">
        <h1> Hướng dẫn chơi </h1>
        <p>
          Nhiệm vụ của bạn là làm cho các ô đều mang màu xanh nước biển trong số
          lượt nhấn ô nhất định.
        </p>
        <p>
          Mỗi ô khi ấn sẽ làm đổi màu chính ô được nhấn và 4 ô xung quanh nó.
        </p>
      </div>
    );
  }
}

export default Guide;
